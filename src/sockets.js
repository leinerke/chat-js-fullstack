const Chat = require('./models/Chat');

module.exports = (io) => {
  let users = {};

  io.on('connection', async socket => {
    socket.on('send message', async (data, cb) => {
      let msg = data.trim();
      if (msg.substr(0, 3) === '/w ') {
        msg = msg.substr(3);
        const index = msg.indexOf(' ');
        if (index !== -1) {
          let name = msg.substring(0, index);
          msg = msg.substring(index + 1);
          if (name in users) {
            users[name].emit('whisper', {
              msg,
              nick: socket.user,
            });
            users[socket.user].emit('whisper', {
              msg,
              nick: socket.user,
            });
          } else {
            cb('Error! Please enter a valid user');
          }
        } else {
          cb('Error! Please enter your message');
        }
      } else {

        const newMsg = new Chat({
          msg,
          nick: socket.user,
        });
        await newMsg.save();

        io.sockets.emit('new message', {
          msg,
          nick: socket.user,
        });
      }
    });

    const messages = await Chat.find({});
    socket.emit('load old msgs', messages);

    socket.on('new user', (data, cb) => {
      if (data in users) {
        cb(false);
      } else {
        cb(true);
        socket.user = data;
        users[socket.user] = socket;
        updateUsers();
      }
    });
    socket.on('disconnect', data => {
      if (!socket.user) {
        return;
      }
      delete users[socket.user];
      updateUsers();
    });

    function updateUsers() {
      io.sockets.emit('usernames', Object.keys(users));
    }
  });
};