$(function() {
  const socket = io();

  // obtaining DOM elements from the interface
  const $messageForm = $('#message-form');
  const $message = $('#message');
  const $chat = $('#chat');
  const $usernames = $('#usernames');

  // obtaining DOM elements from the nicknameForm
  const $nickForm = $('#nickForm');
  const $nickName = $('#nickName');
  const $nickError = $('#nickError');

  $nickForm.submit(e => {
    e.preventDefault();

    socket.emit('new user', $nickName.val(), data => {
      if (data) {
        $('#nickWrap').hide();
        $('#contentWrap').show();
        $message.focus();
      } else {
        const $nickErrorText = 'That username already exits.';
        $nickError
            .html(`<div class="alert alert-danger">${$nickErrorText}</div>`);
      }
      $nickName.val('');
    });
  });

  // events
  $messageForm.submit(e => {
    e.preventDefault();
    socket.emit('send message', $message.val(), data => {
      $chat.append(`<p class="error">${data}</p>`);
    });
    $message.val('');
    $message.focus();
  });

  socket.on('new message', ({
    msg,
    nick,
  }) => {
    $chat.append(`<p><b>${nick}</b>: ${msg}</p>`);
  });

  socket.on('whisper', ({
    msg,
    nick,
  }) => {
    $chat.append(`<p class="whisper"><b>${nick}</b>: ${msg}</p>`);
  });

  socket.on('usernames', data => {
    let html = '';
    data.map(username => {
      html += `<p><i class="fas fa-user"></i> ${username}</p>`;
    });
    $usernames.html(html);
  });

  socket.on('load old msgs', msgs => {
    msgs.map(({
      nick,
      msg,
    }) => {
      $chat.append(`<p class="old"><b>${nick}</b>: ${msg}</p>`);
    });
  });

  function displayMsg(msg) {
    $chat.append(`<p><b>${nick}</b>: ${msg}</p>`);
  }
});