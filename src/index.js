const http = require('http');
const path = require('path');
const express = require('express');
const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);
const mongoose = require('mongoose');

//db connection
mongoose.connect('mongodb://127.0.0.1:27017/chat-database', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
    .then(db => console.log('db is connected'))
    .catch(err => console.log(err));

// settings
app.set('port', process.env.PORT || 3000);

require('./sockets')(io);

// static files
app.use(express.static(path.join(__dirname, 'public')));

// starting the server
server.listen(app.get('port'), () => {
  console.log('server on port ' + app.get('port'));
});
